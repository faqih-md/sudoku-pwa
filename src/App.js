import React, { useState, useEffect } from 'react';
import './App.css';
import { sudoku } from './robatron/sudokujs/sudoku';

function getBoard(data) {
  let entries = data.split('');

  let matrix = [];
  let row = 1;
  let column = 1;
  entries.forEach((entry, i) => {
    let r = "r" + row;
    let c = "c" + column;
    let node = {
      key: r + c,
      type: entry === '.' ? 'Q' : 'D', // Question or Data
      num: entry,
      duplicate: 'F' // True or False
    };
    matrix.push(node);

    column++;
    if ((i + 1) % 9 === 0) {
      column = 1;
      row++;
    }
  });

  return matrix;
}

function initBoard() {
  let entries = '_________________________________________________________________________________';
  return getBoard(entries);
}

function startBoard() {
  let entries = sudoku.generate("easy");
  return getBoard(entries);
}

function App() {
  const top_borders = [0, 1, 2];
  const bottom_borders = [6, 7, 8, 15, 16, 17, 24, 25, 26];

  const [matrixs, setMatrixs] = useState([]);
  const [started, setStarted] = useState(false);
  const [seconds, setSeconds] = useState(0);
  const [timerId, setTimerId] = useState(null);
  const [processed, setProcessed] = useState(false);

  const initMatrixs = () => {
    let board = initBoard();
    // console.log(board);
    setMatrixs(board);
  }

  const getMatrixs = () => {
    let board = startBoard();
    // console.log(board);
    setMatrixs(board);
    timerStart();
  }

  const handleChange = (e) => {
    let index = matrixs.findIndex(x => x.key === e.target.name);
    let key = e.target.name;
    let row_number = e.target.name.substring(0, 2);
    let col_number = e.target.name.substring(2, 4);
    let newMatrix = [...matrixs];
    let value = e.target.value;
    if (value.length > 1) {
      value = e.target.value.split('')[e.target.value.length - 1];
    }

    let duplicates = matrixs.filter(entry => (entry.key.includes(row_number) || entry.key.includes(col_number)) && entry.num === value);
    duplicates.forEach(entry => {
      let i = matrixs.findIndex(x => x.key === entry.key);
      newMatrix[i] = { ...newMatrix[i], duplicate: 'T' };
    });

    let valueBefore = matrixs[index].num;
    let duplicatesBefore = matrixs.filter((entry, i) => entry.num === valueBefore && entry.duplicate === 'T' && i !== index);
    duplicatesBefore.forEach(entry => {
      let e_index = matrixs.findIndex(x => x.key === entry.key);
      let e_row = entry.key.substring(0, 2);
      let e_col = entry.key.substring(2, 4);
      let insideDuplicate = duplicatesBefore.filter(x => (x.key.includes(e_row) || x.key.includes(e_col)) && x.key !== entry.key && x.key !== key);
      if (insideDuplicate.length === 0) {
        newMatrix[e_index] = { ...newMatrix[e_index], duplicate: 'F' };
      }
    });

    newMatrix[index] = { ...newMatrix[index], num: value, duplicate: duplicates.length > 0 ? 'T': 'F'};
    setMatrixs(newMatrix);
  }

  const handleBtnStart = () => {
    setProcessed(true);
    setStarted(true);
    setTimeout(gameStarted, 1000);
  }

  const gameStarted = () => {
    setMatrixs([]);
    setProcessed(false);
  }

  const handleBtnStop = () => {
    clearInterval(timerId);
    setStarted(false);
  }

  const timerStart = () => {
    setSeconds(0);
    setTimerId(setInterval(timerCallback, 1000));
  }

  const timerCallback = () => {
    setSeconds(seconds => seconds + 1);
  }

  useEffect((effect) => {
    if (matrixs.length === 0 && !started) {
      initMatrixs();
    }

    if (matrixs.length === 0 && started) {
      getMatrixs();
    }

    if (matrixs.length > 0 && matrixs.filter(x => x.type === 'Q').length > 0 && matrixs.filter(x => x.num === '.' || x.num === '' || x.duplicate === 'T').length === 0) {
      handleBtnStop();
    }
  });

  return (
    <div className="container">
      <nav className="navbar">
        <div className="navbar-brand">Sudoku PWA</div>
      </nav>
      <div className="container board">
        {
          matrixs.filter(x => x.num === '_').length > 0 &&
          <div className="w-100 text-end fw-bold">00:00</div>
        }
        {
          matrixs.filter(x => x.num === '_').length === 0 &&
          <div className="w-100 text-end fw-bold">
            {seconds / 60 >= 1 ? (Math.floor(seconds / 60)).toString().padStart(2, "0") : "00"}:{(seconds % 60).toString().padStart(2, "0")}
          </div>
        }
        <div className="row text-center fw-bold mt-2">
          {
            matrixs.reduce((all, one, i) => {
              const ch = Math.floor(i / 3);
              all[ch] = [].concat((all[ch] || []), one);
              return all
            }, []).map((matrix, i) =>
              <div key={i} className={"col-4 vertical-border" + (bottom_borders.includes(i) ? " bottom-border" : top_borders.includes(i) ? " top-border" : "")}>
                <div className="row">
                  {
                    matrix.map((entry, i) =>
                      <div key={entry.key} className={(entry.type !== 'Q' ? "col-4 card" : "col-4 card card-form") + (entry.duplicate === 'T' ? " bg-warning" : "")}>
                        <div className="card-body">
                          {
                            entry.type !== 'Q' &&
                            <span>{entry.num}</span>
                          }
                          {
                            entry.type === 'Q' &&
                            <input name={entry.key} type="number" disabled={!started ? " disabled" : ""} className={"form-control text-center fw-bold"} onChange={e => handleChange(e)} value={isNaN(entry.num)?'':entry.num.toString()} />
                          }
                        </div>
                      </div>
                    )
                  }
                </div>
              </div>
            )
          }
        </div>
        {
          !started &&
          <div className="container mt-3 text-center">
            <button type="button" className="btn btn-primary" onClick={handleBtnStart}>{matrixs.filter(x => x.num === '_').length > 0 ? "Start" : "Reset"}</button>
          </div>
        }
        {
          started &&
          <div className="container mt-3 text-center">
            <button type="button" className="btn btn-primary" onClick={handleBtnStop}>Stop</button>
          </div>
        }
      </div>

      <div className={"custom-modal" + (!processed ? " modal fade" : "")}>
        <div className="modal-dialog modal-dialog-centered align-items-center" role="document">
          <div className="modal-content align-items-center loader-body">
            <div className="loader"></div>
          </div>
        </div>
      </div>

      <div className="footer pt-3 w-100 text-center">
        <a href="https://faqih-md.web.app" target="_blank" rel="noreferrer" >faqih-md</a>
      </div>
    </div>
  );
}

export default App;